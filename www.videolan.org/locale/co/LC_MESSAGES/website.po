# Corsican translation
# Copyright (C) 2021 VideoLAN
# This file is distributed under the same license as the vlc package.
#
# Translators:
# Patriccollu <Patrick.Santa-Maria@laposte.net>, 2015-2019
msgid ""
msgstr ""
"Project-Id-Version: VideoLAN's websites\n"
"Report-Msgid-Bugs-To: vlc-devel@videolan.org\n"
"POT-Creation-Date: 2020-02-05 18:13+0100\n"
"PO-Revision-Date: 2019-07-06 19:03+0200\n"
"Last-Translator: Patriccollu <Patrick.Santa-Maria@laposte.net>, 2019\n"
"Language-Team: Corsican (http://www.transifex.com/yaron/vlc-trans/language/"
"co/)\n"
"Language: co\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: include/header.php:289
msgid "a project and a"
msgstr "un prughjettu è un"

#: include/header.php:289
msgid "non-profit organization"
msgstr "urganismu senza prufittu"

#: include/header.php:298 include/footer.php:79
msgid "Partners"
msgstr "Cumpagni"

#: include/menus.php:32
msgid "Team &amp; Organization"
msgstr "Squadra è Urganismu"

#: include/menus.php:33
msgid "Consulting Services &amp; Partners"
msgstr "Servizii di cunsigliu è Cumpagni"

#: include/menus.php:34 include/footer.php:82
msgid "Events"
msgstr "Evvenimenti"

#: include/menus.php:35 include/footer.php:77 include/footer.php:111
msgid "Legal"
msgstr "Infurmazioni legale"

#: include/menus.php:36 include/footer.php:81
msgid "Press center"
msgstr "Centru di stampa"

#: include/menus.php:37 include/footer.php:78
msgid "Contact us"
msgstr "Cuntattatecci"

#: include/menus.php:43 include/os-specific.php:279
msgid "Download"
msgstr "Scaricà"

#: include/menus.php:44 include/footer.php:35
msgid "Features"
msgstr "Funzioni"

#: include/menus.php:45 vlc/index.php:57 vlc/index.php:63
msgid "Customize"
msgstr "Persunalizà"

#: include/menus.php:47 include/footer.php:69
msgid "Get Goodies"
msgstr "Ottene rigali"

#: include/menus.php:51
msgid "Projects"
msgstr "Prughjetti"

#: include/menus.php:71 include/footer.php:41
msgid "All Projects"
msgstr "Tutti i prughjetti"

#: include/menus.php:75 index.php:168
msgid "Contribute"
msgstr "Cuntribuì"

#: include/menus.php:77
msgid "Getting started"
msgstr "Principià pianu pianu"

#: include/menus.php:78 include/menus.php:96
msgid "Donate"
msgstr "Rigalà"

#: include/menus.php:79
msgid "Report a bug"
msgstr "Riferisce un penseru"

#: include/menus.php:83
msgid "Support"
msgstr "Assistenza"

#: include/footer.php:33
msgid "Skins"
msgstr "Vestiture"

#: include/footer.php:34
msgid "Extensions"
msgstr "Stinzioni"

#: include/footer.php:36 vlc/index.php:83
msgid "Screenshots"
msgstr "Catture di screnu"

#: include/footer.php:61
msgid "Community"
msgstr "Cumunità"

#: include/footer.php:64
msgid "Forums"
msgstr "Fori"

#: include/footer.php:65
msgid "Mailing-Lists"
msgstr "Liste di diffusione"

#: include/footer.php:66
msgid "FAQ"
msgstr "FAQ"

#: include/footer.php:67
msgid "Donate money"
msgstr "Rigalà qualchì soldu"

#: include/footer.php:68
msgid "Donate time"
msgstr "Rigalà di u so tempu"

#: include/footer.php:75
msgid "Project and Organization"
msgstr "Prughjettu è Urganismu"

#: include/footer.php:76
msgid "Team"
msgstr "Squadra"

#: include/footer.php:80
msgid "Mirrors"
msgstr "Spechji"

#: include/footer.php:83
msgid "Security center"
msgstr "Centru di sicurità"

#: include/footer.php:84
msgid "Get Involved"
msgstr "Implicassi"

#: include/footer.php:85
msgid "News"
msgstr "Nutizie"

#: include/os-specific.php:103
msgid "Download VLC"
msgstr "Scaricà VLC"

#: include/os-specific.php:109 include/os-specific.php:295 vlc/index.php:168
msgid "Other Systems"
msgstr "Altri sistemi"

#: include/os-specific.php:260
msgid "downloads so far"
msgstr "scaricamenti sin’avà"

#: include/os-specific.php:648
msgid ""
"VLC is a free and open source cross-platform multimedia player and framework "
"that plays most multimedia files as well as DVDs, Audio CDs, VCDs, and "
"various streaming protocols."
msgstr ""
"VLC hè un sunatore multimedia liberu è « fonte aperta » per tutti i sistemi "
"è una struttura chì suna guasi tutti i schedarii multimedia ma dinù DVD, CD "
"Audio, VCD, è parechji protocolli di lettura di cuntinuu."

#: include/os-specific.php:652
msgid ""
"VLC is a free and open source cross-platform multimedia player and framework "
"that plays most multimedia files, and various streaming protocols."
msgstr ""
"VLC hè un sunatore multimedia liberu è « fonte aperta » per tutti i sistemi "
"è una struttura chì suna guasi tutti i schedarii multimedia è parechji "
"protocolli di lettura di cuntinuu."

#: index.php:4
msgid "VLC: Official site - Free multimedia solutions for all OS!"
msgstr ""
"VLC : Situ ufficiale - Suluzioni libere di multimedia per tutti i sistemi !"

#: index.php:26
msgid "Other projects from VideoLAN"
msgstr "Altri prughjetti da VideoLAN"

#: index.php:30
msgid "For Everyone"
msgstr "Per tutti"

#: index.php:40
msgid ""
"VLC is a powerful media player playing most of the media codecs and video "
"formats out there."
msgstr ""
"VLC hè un sunatore multimedia putente chì suna guasi tutti i codecs media è "
"i furmati video chì esistenu."

#: index.php:53
msgid ""
"VideoLAN Movie Creator is a non-linear editing software for video creation."
msgstr ""
"VideoLAN Movie Creator hè un prugramma non-lineare di mudificazione per e "
"creazioni video."

#: index.php:62
msgid "For Professionals"
msgstr "Per i prufessiunali"

#: index.php:72
msgid ""
"DVBlast is a simple and powerful MPEG-2/TS demux and streaming application."
msgstr ""
"DVBlast hè un appiecazione simplice è putente per a trasfurmazione « demux » "
"è a lettura di cuntinuu di MPEG-2/TS."

#: index.php:82
msgid ""
"multicat is a set of tools designed to easily and efficiently manipulate "
"multicast streams and TS."
msgstr ""
"multicat hè un inseme d’attrezzi creati per manighjà facilmente è "
"efficentemente i flussi multicast è i TS."

#: index.php:95
msgid ""
"x264 is a free application for encoding video streams into the H.264/MPEG-4 "
"AVC format."
msgstr ""
"x264 hè un appiecazione libera per trasfurmà i flussi video streams in u "
"furmatu H.264/MPEG-4 AVC."

#: index.php:104
msgid "For Developers"
msgstr "Per i sviluppatori"

#: index.php:140
msgid "View All Projects"
msgstr "Affissà tutti i prughjetti"

#: index.php:144
msgid "Help us out!"
msgstr "Aiutatecci !"

#: index.php:148
msgid "donate"
msgstr "rigalà"

#: index.php:156
msgid "VideoLAN is a non-profit organization."
msgstr "VideoLAN hè un urganismu senza prufittu."

#: index.php:157
msgid ""
" All our costs are met by donations we receive from our users. If you enjoy "
"using a VideoLAN product, please donate to support us."
msgstr ""
" Tutti i nostri costi sò cuparti da e dunazioni mandate da i nostri "
"utilizatori. S’ella vi piace d’impiegà un pruduttu VideoLAN, pensate à "
"susteneci."

#: index.php:160 index.php:180 index.php:198
msgid "Learn More"
msgstr "Per sapene di più"

#: index.php:176
msgid "VideoLAN is open-source software."
msgstr "VideoLAN hè un prugramma « fonte aperta »."

#: index.php:177
msgid ""
"This means that if you have the skill and the desire to improve one of our "
"products, your contributions are welcome"
msgstr ""
"Vole si dì chì, s’è voi avete a cumpetenza è a vuluntà di megliurà unu di i "
"nostri prudutti, i vostri cuntribuzioni sò i benvenuti."

#: index.php:187
msgid "Spread the Word"
msgstr "Sparghje dapertuttu"

#: index.php:195
msgid ""
"We feel that VideoLAN has the best video software available at the best "
"price: free. If you agree please help spread the word about our software."
msgstr ""
"Ci pare chì VideoLAN hà u più bellu prugramma video à u più bellu prezzu : "
"gratisi. S’è vò site d’accunsentu, aiutateci à sparghje dapertuttu "
"l’infurmazione apprupositu di u nostru prugramma."

#: index.php:215
msgid "News &amp; Updates"
msgstr "Nutizie è Rinnovi"

#: index.php:218
msgid "More News"
msgstr "D’altre nutizie"

#: index.php:222
msgid "Development Blogs"
msgstr "Blogs di sviluppu"

#: index.php:251
msgid "Social media"
msgstr "Media suciale"

#: vlc/index.php:3
msgid "Official download of VLC media player, the best Open Source player"
msgstr ""
"Scaricamentu ufficiale di VLC media player, u più bellu sunatore in fonte "
"aperta"

#: vlc/index.php:21
msgid "Get VLC for"
msgstr "Ottene VLC per"

#: vlc/index.php:29 vlc/index.php:32
msgid "Simple, fast and powerful"
msgstr "Simplice, prontu è putente"

#: vlc/index.php:35
msgid "Plays everything"
msgstr "Suna tuttu"

#: vlc/index.php:35
msgid "Files, Discs, Webcams, Devices and Streams."
msgstr "Schedarii, dischetti, webcams, apparechji (periferichi) è flussi."

#: vlc/index.php:38
msgid "Plays most codecs with no codec packs needed"
msgstr "Suna guasi tutti i codec senza alcunu « pack »"

#: vlc/index.php:41
msgid "Runs on all platforms"
msgstr "Funziona nant’à tutti i sistemi"

#: vlc/index.php:44
msgid "Completely Free"
msgstr "Tuttu gratisi !"

#: vlc/index.php:44
msgid "no spyware, no ads and no user tracking."
msgstr "senza spia, senza publicità è senza seguità l’utilizatori."

#: vlc/index.php:47
msgid "learn more"
msgstr "per sapene di più"

#: vlc/index.php:66
msgid "Add"
msgstr "Aghjunghje"

#: vlc/index.php:66
msgid "skins"
msgstr "vestiture"

#: vlc/index.php:69
msgid "Create skins with"
msgstr "Creà vestiture cù"

#: vlc/index.php:69
msgid "VLC skin editor"
msgstr "VLC skin editor"

#: vlc/index.php:72
msgid "Install"
msgstr "Installà"

#: vlc/index.php:72
msgid "extensions"
msgstr "stinzioni"

#: vlc/index.php:126
msgid "View all screenshots"
msgstr "Affissà tutte e catture di screnu"

#: vlc/index.php:135
msgid "Official Downloads of VLC media player"
msgstr "Scaricamenti ufficiale di VLC media player"

#: vlc/index.php:146
msgid "Sources"
msgstr "Fonte"

#: vlc/index.php:147
msgid "You can also directly get the"
msgstr "Pudete dinù ottene subitu u"

#: vlc/index.php:148
msgid "source code"
msgstr "codice di fonte"

#~ msgid "A project and a"
#~ msgstr "Un prughjettu è un"

#~ msgid ""
#~ "composed of volunteers, developing and promoting free, open-source "
#~ "multimedia solutions."
#~ msgstr ""
#~ "cumposti da vuluntarii, sviluppendu è prumuvendu suluzioni libere per u "
#~ "multimedia."

#~ msgid "why?"
#~ msgstr "perchè ?"

#~ msgid "Home"
#~ msgstr "Accolta"

#~ msgid "Support center"
#~ msgstr "Centru di supportu"

#~ msgid "Dev' Zone"
#~ msgstr "Locu di Sviluppu"

#~ msgid "Simple, fast and powerful media player."
#~ msgstr "Sunatore multimedia simplice, prontu è putente."

#~ msgid "Plays everything: Files, Discs, Webcams, Devices and Streams."
#~ msgstr ""
#~ "Suna tuttu : Schedarii, Dischetti, Webcams, Apparechji (Periferichi) è "
#~ "Flussi."

#~ msgid "Plays most codecs with no codec packs needed:"
#~ msgstr "Suna guasi tutti i codec senza bisognu di 'pack' :"

#~ msgid "Runs on all platforms:"
#~ msgstr "Funziona nantu à tutti i sistemi :"

#~ msgid "Completely Free, no spyware, no ads and no user tracking."
#~ msgstr ""
#~ "Interamente gratisi è liberu, senza spia, senza publicità è senza seguità "
#~ "l'utilizatori."

#~ msgid "Can do media conversion and streaming."
#~ msgstr "Pò fà a cunversione di media è a lettura di cuntinuu."

#~ msgid "Discover all features"
#~ msgstr "Scuprite tutte e funzioni"
